== Textures
If not specified otherwise nodes textures are used from 'Pixel Perfection' texture pack

=== Pixel Perfection
Pixel Perfection by XSSheep is licensed under a Creative Commons Attribution-Share Alike 4.0 International License.
URL: http://www.minecraftforum.net/forums/mapping-and-modding/resource-packs/1242533-pixel-perfection-now-with-polar-bears-1-11

=== Minetest Default Game
URL: https://github.com/minetest/minetest_game
* crack_anylength.png