-------------------------------
-- Zelar player init

-- License: MIT
-- Credits: xeranas
-------------------------------

local modpath = minetest.get_modpath("zelar_player");
dofile(modpath.."/zelar_player.lua")