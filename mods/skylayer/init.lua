-------------------------------
-- Sky Layers: initialization

-- License: MIT
-- Credits: xeranas
-------------------------------

local modpath = minetest.get_modpath("skylayer");
dofile(modpath.."/api.lua")
