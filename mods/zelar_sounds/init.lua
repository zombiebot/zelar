-------------------------------
-- Zelar sounds init

-- License: MIT
-- Credits: xeranas
-------------------------------

local modpath = minetest.get_modpath("zelar_sounds");
dofile(modpath.."/mt_default_sounds.lua")